#
# File: sysmon.py
# Description: A simple system resource monitor for Windows
#
# Copyright (C) 2019 Dann Moore.  All rights reserved.
#
import argparse
import ctypes
from ctypes import *
from ctypes import wintypes
import msvcrt
import time
from datetime import timedelta
from colorama import init


# Map DWORDLONG to ctypes
DWORDLONG = ctypes.c_ulonglong



class MEMORYSTATUSEX(ctypes.Structure):
    # Mimic the MEMORYSTATUSEX struct
    _fields_ = [("dwLength", wintypes.DWORD),
                ("dwMemoryLoad", wintypes.DWORD),
                ("ullTotalPhys", DWORDLONG),
                ("ullAvailPhys", DWORDLONG),
                ("ullTotalPageFile", DWORDLONG),
                ("ullAvailPageFile", DWORDLONG),
                ("ullTotalVirtual", DWORDLONG),
                ("ullAvailVirtual", DWORDLONG),
                ("ullAvailExtendedVirtual", DWORDLONG)]
#


class PROCESS_MEMORY_COUNTERS(ctypes.Structure):
    # Mimic the PROCESS_MEMORY_COUNTERS struct
    _fields_ = [("cb", wintypes.DWORD),
                ("PageFaultCount", wintypes.DWORD),
                ("PeakWorkingSetSize", DWORDLONG),
                ("WorkingSetSize", DWORDLONG),
                ("QuotaPeakPagedPoolUsage", DWORDLONG),
                ("QuotaPagedPoolUsage", DWORDLONG),
                ("QuotaPeakNonPagedPoolUsage", DWORDLONG),
                ("QuotaNonPagedPoolUsage", DWORDLONG),
                ("PagefileUsage", DWORDLONG),
                ("PeakPagefileUsage", DWORDLONG)]
#

  

class SYSTEMTIME(ctypes.Structure):
    # Mimic the SYSTEMTIME struct
    _fields_ = [("wYear", wintypes.WORD),
                ("wMonth", wintypes.WORD),
                ("wDayOfWeek", wintypes.WORD),
                ("wDay", wintypes.WORD),
                ("wHour", wintypes.WORD),
                ("wMinute", wintypes.WORD),
                ("wSecond", wintypes.WORD),
                ("wMilliSeconds", wintypes.WORD)]
#



class FILETIME(ctypes.Structure):
    # Mimic the FILETIME struct

    _fields_ = [('dwLowDateTime', wintypes.DWORD),
                ('dwHighDateTime', wintypes.DWORD)]
#







# Convert a given FILETIME type to a ULARGE_INTEGER type
def FILETIMEtoULARGEINTEGER(ft):
   uli = ctypes.wintypes.ULARGE_INTEGER()
   uli = ft.dwHighDateTime
   uli = uli << 32
   uli = uli | ft.dwLowDateTime

   return(uli)
#





# Set a few globals to hold previous usage times due to the lack of static function variables
prev_idletime = FILETIME()
prev_kerneltime = FILETIME()
prev_usertime = FILETIME()

# Returns the % of cpu utilization
def GetCPUUsage():
   global prev_idletime
   global prev_kerneltime
   global prev_usertime


   # Create some FILETIME structures to hold data returned from Windows API calls
   cur_idletime = FILETIME()
   cur_kerneltime = FILETIME()
   cur_usertime = FILETIME()

   # Save old times as large integers
   prev_totalkerneltime = FILETIMEtoULARGEINTEGER(prev_kerneltime)
   prev_totalusertime = FILETIMEtoULARGEINTEGER(prev_usertime)
   prev_totalidletime = FILETIMEtoULARGEINTEGER(prev_idletime)

   prev_totaltime = ctypes.wintypes.ULARGE_INTEGER()
   prev_totaltime = prev_totalkerneltime + prev_totalusertime 


   # Get current system times
   windll.kernel32.GetSystemTimes(byref(cur_idletime), byref(cur_kerneltime), byref(cur_usertime))


   # Convert current system times to large integers
   cur_totalkerneltime = FILETIMEtoULARGEINTEGER(cur_kerneltime)
   cur_totalusertime = FILETIMEtoULARGEINTEGER(cur_usertime)
   cur_totalidletime = FILETIMEtoULARGEINTEGER(cur_idletime)

   cur_totaltime = ctypes.wintypes.ULARGE_INTEGER()
   cur_totaltime = cur_totalkerneltime + cur_totalusertime

   # Calculate the time differences between current and previous readings
   totaltimediff = ctypes.wintypes.ULARGE_INTEGER()
   totaltimediff = cur_totaltime - prev_totaltime
   totalidlediff = ctypes.wintypes.ULARGE_INTEGER()
   totalidlediff = cur_totalidletime - prev_totalidletime

   # Calculate current CPU usage as a percentage
   cputime = ctypes.wintypes.ULARGE_INTEGER()

   if totaltimediff != 0:
      cputime = 100 * (totaltimediff - totalidlediff) / totaltimediff
   else:
      cputime = 0


   # Lastly, copy the current times into the previous times for the next call
   prev_idletime = cur_idletime
   prev_kerneltime = cur_kerneltime
   prev_usertime = cur_usertime

   return(cputime)
#



# Returns the current system date/time
def GetSystemTime():
   systime = SYSTEMTIME()
   windll.kernel32.GetLocalTime(byref(systime))
   return(systime)
#


# Returns the current system uptime
def GetUptime():
   uptime = ctypes.wintypes.ULARGE_INTEGER()
   uptime = windll.kernel32.GetTickCount64()
   return(uptime)
#


# Returns the current system memory usage
def GetMemoryUsage():
   memusage = MEMORYSTATUSEX()
   memusage.dwLength = sizeof(memusage)
   windll.kernel32.GlobalMemoryStatusEx(byref(memusage))
   return(memusage)
#


# Returns the computer name
def GetComputerName():
   computername = create_string_buffer(255)
   nsize = ctypes.wintypes.DWORD(len(computername))
   windll.kernel32.GetComputerNameA(byref(computername), byref(nsize))
   return(computername.value.decode("utf-8"))
#


# Returns the currently logged on username
def GetUserName():
   username = create_string_buffer(255)
   nsize = ctypes.wintypes.DWORD(len(username))
   windll.advapi32.GetUserNameA(byref(username), byref(nsize))
   return(username.value.decode("utf-8"))
#
    


# Returns a list of currently running processes and associated information
def GetProcessList():
   # Create a list of processes to return
   retarray = []

   # Define a couple of flags
   PROCESS_QUERY_INFORMATION = 0x0400
   PROCESS_VM_READ = 0x0010

   # Create a block of memory large enough to hold the Process ID array
   lpidProcess = (ctypes.wintypes.DWORD * 256)()
   cb = ctypes.wintypes.DWORD()
   cb = sizeof(lpidProcess)
   lpcbNeeded = ctypes.wintypes.DWORD()

   # Get a list of pids (Process IDs)
   windll.psapi.EnumProcesses(byref(lpidProcess), cb, byref(lpcbNeeded))


   # Determine how many processes were returned
   num_pids = lpcbNeeded.value // sizeof(ctypes.wintypes.DWORD())

   # Loop through processes
   for x in range(num_pids):
      # Get a handle to this process
      hProcess = windll.kernel32.OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, False, lpidProcess[x])
      # OpenProcess returns NULL on the SYSTEM or Idle processes, or if there was an error.  
      # One common reason is an ACCESS_DENIED error when querying processes running under another account
      if hProcess: 
         # Get module associated with this process
         hModule = ctypes.wintypes.DWORD()
         cb = sizeof(hModule)
         lpcbNeeded = ctypes.wintypes.DWORD()
         windll.psapi.EnumProcessModules(hProcess, byref(hModule), cb, byref(lpcbNeeded))

         # Get the name for the module
         baseName = create_string_buffer(64)
         nSize = sizeof(baseName)
         windll.psapi.GetModuleBaseNameA(hProcess, hModule.value, baseName, nSize)

         # Get the memory usage for this handle
         ppsmemCounters = PROCESS_MEMORY_COUNTERS()
         cb = sizeof(ppsmemCounters)
         windll.psapi.GetProcessMemoryInfo(hProcess, byref(ppsmemCounters), cb)

         retarray.append({"pid": lpidProcess[x], "name": baseName.value.decode("utf-8"), "mem":ppsmemCounters.WorkingSetSize // 1024})
            
         # Close the handle to this process
         windll.kernel32.CloseHandle(hProcess)


   return(retarray)
#




# Use ANSI escape sequences to draw our output in the terminal
def DisplayInfo(username, computername, systime, uptime, memusage, cpuusage, processlist):
   # Clear the screen
   print("\033[2J", end="")

   # Draw the top header bar
   print("\033[44m\033[1m", end="")
   for x in range(80):
      print(" ", end="")

   # Left-justify username
   strlen = len(computername)
   print("\033[31m\033[1;1H" + username, end="")


   # Center computer name and uptime
   timesecs = uptime // 1000
   tmpstr = computername + " (Uptime: " + "{:0>8}".format(str(timedelta(seconds=timesecs))) + ")"
   strlen = len(tmpstr)

   # Convert uptime from ms to seconds
   print("\033[31m\033[1;" + str(40 - (strlen // 2)) + "H" + tmpstr, end="")




   # Right-justify date/time
   timestr = "{:02d}".format(system_time.wYear) + "." + "{:02d}".format(system_time.wMonth) + "." + "{:02d}".format(system_time.wDay) + " @ " + "{:02d}".format(system_time.wHour) + ":" + "{:02d}".format(system_time.wMinute) + ":" + "{:02d}".format(system_time.wSecond)

   print("\033[31m\033[1;" + str(80 - len(timestr) + 1) + "H" + timestr, end="")


   # Draw separator
   print("\033[37m\033[2;1H", end="")
   for x in range(80):
      print("-", end="")


   # Draw cpu usage
   if cpuusage < 50: # Colorize green
      print("\033[32m\033[3;1HCPU: [", end="")
   elif cpuusage < 75: # Colorize yellow
      print("\033[33m\033[3;1HCPU: [", end="")
   else: # Colorize red
      print("\033[31m\033[3;1HCPU: [", end="")

   num_chars = int(cpuusage / 100 * 73)
   for x in range(num_chars):
      print("X", end="")

   for x in range(73 - num_chars):
      print(".", end="")

   print("]", end="")


   # Draw memory usage
   if cpuusage < 50: # Colorize green
      print("\033[32m\033[4;1HMEM: [", end="")
   elif cpuusage < 75: # Colorize yellow
      print("\033[33m\033[4;1HMEM: [", end="")
   else: # Colorize red
      print("\033[31m\033[4;1HMEM: [", end="")

   num_chars = int(memusage.dwMemoryLoad / 100 * 73)
   for x in range(num_chars):
      print("X", end="")

   for x in range(73 - num_chars):
      print(".", end="")

   print("]", end="")




   # Draw separator
   print("\033[37m\033[5;1H", end="")
   for x in range(80):
      print("-", end="")

   # Draw processes
   print("\033[31m\033[45m\033[6;1H", end="")
   for x in range(80):
      print(" ", end="")
   print("\033[33m\033[45m\033[6;1HPID", end="")
   print("\033[33m\033[45m\033[6;9HNAME", end="")
   print("\033[33m\033[45m\033[6;30HMEM", end="")
   print("\033[33m\033[45m\033[6;41HPID", end="")
   print("\033[33m\033[45m\033[6;49HNAME", end="")
   print("\033[33m\033[45m\033[6;70HMEM", end="")
   x = 1
   for proc in processlist:
      if(x <= 17): # column 1
         print("\033[33m\033[45m\033[" + str(x + 6) + ";1H", end="")
         print("\033[33m\033[45m\033[" + str(x + 6) + ";1H" + str(proc["pid"]), end="")
         print("\033[33m\033[45m\033[" + str(x + 6) + ";9H" + str(proc["name"]), end="")
         print("\033[33m\033[45m\033[" + str(x + 6) + ";30H" + str(proc["mem"]) + "K", end="")
      else:
         print("\033[33m\033[45m\033[" + str(x + 6 - 17) + ";1H", end="")
         print("\033[33m\033[45m\033[" + str(x + 6 - 17) + ";41H" + str(proc["pid"]), end="")
         print("\033[33m\033[45m\033[" + str(x + 6 - 17) + ";49H" + str(proc["name"]), end="")
         print("\033[33m\033[45m\033[" + str(x + 6 - 17) + ";70H" + str(proc["mem"]) + "K", end="")
        
      x = x + 1
#









# Initialize colorama for display
init()

# Initialize system times
windll.kernel32.GetSystemTimes(byref(prev_idletime), byref(prev_kerneltime), byref(prev_usertime))


# Parse command-line arguments
displaydelay = 1

parser = argparse.ArgumentParser()
parser.add_argument('-r', action='store', help="Poll system information at the specified rate, in millseconds. (default: 1000ms)", type=int)
args = parser.parse_args()

if args.r:
   displaydelay = float(args.r) / 1000



# Get some static system information
computer_name = GetComputerName()
user_name = GetUserName()



# Loop until ESC is pressed to get real-time system information
while True:
   # Retrieve system information
   system_time = GetSystemTime()
   up_time = GetUptime()
   memory_usage = GetMemoryUsage()
   cpu_usage = GetCPUUsage()
   process_list = GetProcessList()

   # Display information to the screen
   DisplayInfo(user_name, computer_name, system_time, up_time, memory_usage, cpu_usage, process_list)

   # Delay the specified amount
   time.sleep(displaydelay)

   # Check for keyboard input to exit program with the ESC key
   while msvcrt.kbhit(): # Flush all keyboard input except ESC key
      if ord(msvcrt.getch()) == 27:
         quit()






