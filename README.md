# SysMon

<img src="https://gitlab.com/dannmoore/sysmon/raw/master/media/sysmon.png" alt="screenshot" width="640"/>

### Overview:

SysMon is a simple system monitor that uses Windows API calls to gather system information.  SysMon is compatible with Python 3.0 or above running on Windows Vista or above.

### Usage:

Run "python.exe sysmon.py" to use the default refresh rate.

Use "python.exe sysmon.py -r <value>" to specify a custom refresh rate.

Press the ESC key to exit the program.

### Legal:

Copyright (C) 2024 Dann Moore.  All rights reserved.

Colorama is Copyright (C) 2013 Jonathan Hartley, licensed under the BSD 3-Clause license, as originally found at https://github.com/tartley/colorama



